#!/bin/bash

# This script automatically exports the proper flags before installing
# https://github.com/Blizzard/node-rdkafka#mac-os-high-sierra

UNAME=`uname`

echo $UNAME

if [[ "$UNAME" == "Darwin" ]]; then
  export CPPFLAGS=-I/usr/local/opt/openssl/include
  export LDFLAGS=-L/usr/local/opt/openssl/lib
fi
