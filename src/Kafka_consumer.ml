
module Message = struct
  type t = {
    value: Node.buffer;
    size: int;
    topic: string;
    offset: int;
    partition: int;
    key: string;
    timestamp: int;
  }

  type handler = (exn Js.Nullable.t -> t Js.Nullable.t -> unit)
end

module TopicConfig = struct
  type t
end

module GlobalConfig = struct
  type t
end

module ConsumerConfig = struct
  type t = {
    topics: string array
  }
end

type t

external make : ConsumerConfig.t -> t = "KafkaConsumer"
[@@bs.module "node-rdkafka"]

external commit : t -> unit = "" [@@bs.send]

external commitToPartition : t -> int -> unit = "commit" [@@bs.send]

external commitMessage : t -> Message.t -> unit = "" [@@bs.send]

external connect : t -> unit = "" [@@bs.send]

external consume :
  t ->
  (exn Js.Nullable.t -> Message.t Js.Nullable.t -> unit) ->
  unit = ""
  [@@bs.send]

external consume_count :
  t ->
  int ->
  (exn Js.Nullable.t -> Message.t Js.Nullable.t -> unit) ->
  unit = "consume"
  [@@bs.send]

external disconnect : t -> unit = "" [@@bs.send]

(*
 * @TODO - most of the even handler input types are wrong, figure out what
 * they should be and correct them.
 *)
external on :
  t ->
  ([
   | `data of Message.t -> unit
   | `disconnected of unit -> unit
   | `event of unit -> unit
   | `log of unit -> unit [@bs.as "event.log"]
   | `ready of unit -> unit
   | `stats of unit -> unit [@bs.as "event.stats"]
   | `throttle of unit -> unit [@bs.as "event.throttle"]
   ][@bs.string]) ->
  t = ""
  [@@bs.send]

external metadataGet : t -> Kafka_metadata.t = "getMetadata" [@@bs.send]

type options = {
  topic: string;
  timeout: int;
}

external metadata_topic : t -> options -> Kafka_metadata.t = "getMetadata"
[@@bs.send]

external query_water_mark_offsets :
  t ->
  string ->
  int ->
  int ->
  (exn Js.Nullable.t -> Kafka_offet.t Js.Nullable.t -> unit) ->
  unit = "queryWatermarkOffsets"
  [@@bs.send]

external subscribe : t -> string array -> unit = "" [@@bs.send]

external unsubscribe : t -> unit = "" [@@bs.send]


module ReadStream = struct
  type t

  type builder

  external load : unit -> builder = "KafkaConsumer"
  [@@bs.module "node-rdkafka"]

  external create_read_stream :
    builder ->
    GlobalConfig.t ->
    TopicConfig.t ->
    ConsumerConfig.t ->
    t = "createReadStream"
    [@@bs.send]

  external on :
    t ->
    ([
     | `data of Message.t -> unit
     ] [@bs.string]) ->
    t = ""
    [@@bs.send]

  (*
   * @TODO - figure out access to the stream consumer object
   *)

let make global topic consumer =
  load ()
  |. create_read_stream global topic consumer

end
