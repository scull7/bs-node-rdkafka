(*
 * # Kafka Consumer
 *)

module Message : sig
  type t = {
    value: Node.buffer;
    size: int;
    topic: string;
    offset: int;
    partition: int;
    key: string;
    timestamp: int;
  }

  type handler = (exn Js.Nullable.t -> t Js.Nullable.t -> unit)
end

(*
 * ## Kafka Consumer Configuration
 *)
module TopicConfig : sig
  type t
end

module GlobalConfig : sig
  type t
end

module ConsumerConfig : sig
  type t = {
    topics: string array
  }
end

type t

val make : ConsumerConfig.t -> t

val commit : t -> unit

val commitToPartition : t -> int -> unit

val commitMessage : t -> Message.t -> unit

val connect : t -> unit

val consume : t -> Message.handler -> unit

val consume_count : t -> int -> Message.handler -> unit

val disconnect : t -> unit

val on :
  t ->
  ([
   | `data of Message.t -> unit
   | `disconnected of unit -> unit
   | `event of unit -> unit
   | `log of unit -> unit
   | `ready of unit -> unit
   | `stats of unit -> unit
   | `throttle of unit -> unit
   ]) ->
  t

val subscribe : t -> string array -> unit

val unsubscribe : t -> unit

(*
 * ### Get Metadata
 * Retrieve metadata about the Kafka connection.
 *)
val metadataGet : t -> Kafka_metadata.t

(*
 * ### Get Metadata for a specific topic
 * When fetching metadata for a specific topic, if a topic reference does not
 * exist, one is created using the default config.  Please see the
 * documentation on the `Client.metadataGet` if you want to set configuration
 * parameters, e.g. `acks`, on a topic to produce messages to.
 *)
type options = {
  topic: string;
  timeout: int;
}

val metadata_topic : t -> options -> Kafka_metadata.t

(*
 * ### Reading current offets from the broker for a topic
 * Some times you find yourself in the situation where you need to know the
 * latest (and earliest) offset for one of your topics.  Connected producers
 * and consumers both allow you to query for these through the 
 * `queryWaterMarkOffsets`
 *)
val query_water_mark_offsets :
  t ->
  string ->
  int -> 
  int ->
  (exn Js.Nullable.t -> Kafka_offet.t Js.Nullable.t -> unit) ->
  unit


module ReadStream : sig
  type t

  val make : GlobalConfig.t -> TopicConfig.t -> ConsumerConfig.t -> t

  val on : t -> ([ | `data of Message.t -> unit ]) -> t
end
