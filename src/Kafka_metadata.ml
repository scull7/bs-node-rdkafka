module Broker = struct
  type t = {
    id: int;
    host: string;
    port: int;
  } [@@bs.deriving abstract]
end

module Topic = struct

  module Partition = struct
    type t = {
      id: int;
      leader: int;
      replicas: int list;
      isrs: int list;
    } [@@bs.deriving abstract]
  end

  type t = {
    name: string;
    partitions: Partition.t array
  } [@@bs.deriving abstract]
end

type t = {
  orig_broker_id: int;
  orig_broker_name: string;
  brokers: Broker.t array;
  topics: Topic.t array;
} [@@bs.deriving abstract]
