type t = {
  high: int [@bs.as "highOffet"];
  low: int [@bs.as "lowOffset"];
} [@@bs.deriving abstract]
