module Config = struct
  type t = {
    batch_num_messages: int
    [@bs.as "batch.num.messages"][@bs.optional];
    broker_list: string
    [@bs.as "metadata.broker.list"];
    client_id: string
    [@bs.as "client.id"][@bs.optional];
    compression_codec: string
    [@bs.as "compression.codec"][@bs.optional];
    event_delivery_report: bool
    [@bs.as "dr_cb"][@bs.optional];
    message_send_max_retries: int
    [@bs.as "message.send.max.retries"][@bs.optional];
    queue_buffering_max_messages: int
    [@bs.as "queue.buffering.max.messages"][@bs.optional];
    queue_biffering_max_ms: int
    [@bs.as "queue.buffering.max.ms"][@bs.optional];
    retry_backoff_ms: int
    [@bs.as "retry.backoff.ms"][@bs.optional];
    socket_keepalive_enable: bool
    [@bs.as "socket.keepalive.enable"][@bs.optional];
  } [@@bs.deriving abstract]
end

type t

(*
 * @TODO - figure out what the report object looks like.
 *)
module Report = struct
  type t
end

external make : Config.t -> t = "Producer"
[@@bs.new] [@@bs.module "node-rdkafka"]

external connect : t -> unit = "" [@@bs.send]

external disconnect : t -> unit = "" [@@bs.send]

external flush : t -> int -> (unit -> unit) -> unit = "" [@@bs.send]

external metadataGet : t -> Kafka_metadata.t = "getMetadata"
[@@bs.send]

type options = {
  topic: string;
  timeout: int;
}

external metadata_topic : t -> options -> Kafka_metadata.t = "getMetadata"
[@@bs.send]

(*
 * @TODO - most of the even handler input types are wrong, figure out what
 * they should be and correct them.
 *)
external on :
  t ->
  ([
   | `delivery of Report.t -> unit [@bs.as "delivery-report"]
   | `disconnected of unit -> unit
   | `event of unit -> unit
   | `error of exn -> unit [@bs.as "event.error"]
   | `log of unit -> unit [@bs.as "event.log"]
   | `ready of unit -> unit
   | `stats of unit -> unit [@bs.as "event.stats"]
   | `throttle of unit -> unit [@bs.as "event.throttle"]
  ] [@bs.string]) ->
  t = ""
  [@@bs.send]

external poll : t -> unit = "" [@@bs.send]

external produce :
  t ->
  (* Topic to send the message to *)
  string ->
  int Js.Nullable.t ->
  Node.buffer ->
  string ->
  Js.Date.t ->
  Js.Json.t option ->
  unit = ""
  [@@bs.send]

external query_water_mark_offsets :
  t ->
  string ->
  int ->
  int ->
  (exn Js.Nullable.t -> Kafka_offet.t Js.Nullable.t -> unit) ->
  unit = "queryWatermarkOffsets"
  [@@bs.send]

external setPollInterval : t -> int -> unit = "" [@@bs.send]

module WriteStream = struct
  type t

  type builder

  external load : unit -> builder = "Producer"
  [@@bs.module "node-rdkafka"]

  external create_write_stream : builder -> Config.t -> t
  = "createWriteStream"
  [@@bs.send]

  external on :
    t ->
    ([
     | `error of exn -> unit
    ] [@bs.string])
  -> t = "" [@@bs.send]

  external write : t -> Node.buffer -> bool = "" [@@bs.send]

  let make config = () |. load |. create_write_stream config

end
