(* # Kafka Producer
 *
 *)

(*
 * ## Kafka Producer Configuration
 *)
module Config : sig

  type t = {
    batch_num_messages: int
    [@bs.as "batch.num.messages"][@bs.optional];
    broker_list: string
    [@bs.as "metadata.broker.list"];
    client_id: string
    [@bs.as "client.id"][@bs.optional];
    compression_codec: string
    [@bs.as "compression.codec"][@bs.optional];
    event_delivery_report: bool
    [@bs.as "dr_cb"][@bs.optional];
    message_send_max_retries: int
    [@bs.as "message.send.max.retries"][@bs.optional];
    queue_buffering_max_messages: int
    [@bs.as "queue.buffering.max.messages"][@bs.optional];
    queue_biffering_max_ms: int
    [@bs.as "queue.buffering.max.ms"][@bs.optional];
    retry_backoff_ms: int
    [@bs.as "retry.backoff.ms"][@bs.optional];
    socket_keepalive_enable: bool
    [@bs.as "socket.keepalive.enable"][@bs.optional];
  } [@@bs.deriving abstract]

end

module Report : sig
  type t
end

type t

(*
 * ## Kafka Producer Constructor
 *
 *)
val make : Config.t -> t

(*
 * ### producer.connect()
 * Connects to the broker
 *
 * The `connect()` method emits the `ready` event when it connects
 * successfully.  If it does not, the error will be passed through the 
 * callback.
 *)
val connect : t -> unit

(*
 * ### producer.disconnect()
 * Disconnects from the broker.
 *
 * The `disconnect()` method emits the `disconnected` event when it has
 * disconnected.  If it does not, the error will be passed through the
 * callback.
 *)
val disconnect : t -> unit

(*
 * ### producer.poll()
 * Polls the producer for delivery reports or other events to be transmitted
 * via the emitter.
 *
 * In order to get the events in `librdkafka`'s queue to emit you must call
 * this regularly.
 *)
val poll : t -> unit

(*
 * ### producer.setPollInterval(interval)
 * Pollls the producer on this interval, handling diconnections and
 * reconnection.  Set it to zero (0) to turn it off.
 *)
val setPollInterval : t -> int -> unit

(*
 * ### producer.produce(topic, partition, msg, key, timestamp, opaque)
 * Sends a message.
 *
 * The `produce()` method throws when produce would return an error.
 * Ordinarily, this is just if the queue is full.
 *)
val produce :
  t ->
  string ->
  int Js.Nullable.t ->
  Node.buffer ->
  string ->
  Js.Date.t ->
  Js.Json.t option ->
  unit

(*
 * ### producer.flush(timeout, callback)
 * Flush the librdkafka internal queue, sending all messages.
 * Default timeout is 500ms
 *)
val flush : t -> int -> (unit -> unit) -> unit

(*
 * ### Event Handler
 *
 * * _disconnected_ - The `disconnected` event is emitted when the broker has
 *     disconnected.  This event is emitted only when `disconnect()` is called.
 *     The wrapper will always try to reconnect otherwise.
 * * _ready_ - The `ready` event is emitted when the `Producer` is ready to send
 *     messages.
 * * _event_ - The `event` is emitted when `librdkafka` reports an event (if 
 *     you've opted in via the `event_cb` option).
 * * _log_ - The `event.log` event is emitted when logging events com in (if
 *     you'v opted in via the event_cb options).  You will need to set a value
 *     for `debug` if you want to send information.
 * * _stats_ - The `event.stats` event is emitted when `librdkafka` reports
 *     stats (if you've opted in by setting the `statistics.interval.ms` to a 
 *     non-zero value).
 * * _error_ - The `event.error` event is emitted when `librdkafka` reports
 *     an error.
 * * _throttle_ - The `event.throttle` event is emitted when `librdkafka`
 *     reports throttling.
 * * _delivery_ - The `delivery-report` event is emitted when a delivery
 *     report has been found via polling.  To use this event you must set
 *     `request.required.acks` to `1` or `-1` in topic configuration and
 *     `dr_cb` (or `dr_msg_cb` if you want the report to contain the message
 *     payload) to `true` in the `Producer` constructor options.
 *)
val on : 
  t ->
  ([
   | `delivery of Report.t -> unit
   | `disconnected of unit -> unit
   | `error of exn -> unit
   | `event of unit -> unit
   | `log of unit -> unit
   | `ready of unit -> unit
   | `stats of unit -> unit
   | `throttle of unit -> unit
   ]) ->
  t

(*
 * ### Get Metadata
 * Retrieve metadata about the Kafka connection.
 *)
val metadataGet : t -> Kafka_metadata.t

(*
 * ### Get Metadata for a specific topic
 * When fetching metadata for a specific topic, if a topic reference does not
 * exist, one is created using the default config.  Please see the
 * documentation on the `Client.metadataGet` if you want to set configuration
 * parameters, e.g. `acks`, on a topic to produce messages to.
 *)
type options = {
  topic: string;
  timeout: int;
}

val metadata_topic : t -> options -> Kafka_metadata.t

(*
 * ### Reading current offets from the broker for a topic
 * Some times you find yourself in the situation where you need to know the
 * latest (and earliest) offset for one of your topics.  Connected producers
 * and consumers both allow you to query for these through the 
 * `queryWaterMarkOffsets`
 *)
val query_water_mark_offsets :
  t ->
  string ->
  int -> 
  int ->
  (exn Js.Nullable.t -> Kafka_offet.t Js.Nullable.t -> unit) ->
  unit

(*
 * ## Stream API
 * You can easily use the Producer as a writable stream immediately after
 * creation.
 *)
module WriteStream : sig
  type t

  (*
   * ### Constructor
   * Construct a writable stream which will stream messages to Kafka.
   *)
  val make : Config.t -> t

  (*
   * ### Stream Event Handler
   * * _error_ - The `error` event wil notify us if something has gone wrong
   *     while attempting to write Kafka.
   *)
  val on : t -> ([ | `error of exn -> unit ]) -> t

  (*
   * ### Write
   * Write a message to Kafka.
   *)
  val write : t -> Node.buffer -> bool
end
